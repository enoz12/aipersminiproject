package com.aipers.moon.domain.member.service;

import com.aipers.moon.domain.member.contans.AuthorityType;
import com.aipers.moon.domain.member.entity.Member;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MemberServiceTest {
    @Autowired
    MemberService memberService;

    @Test
    public void 맴버저장(){
        Member buildMemberInfo = Member.builder()
                                        .name("문명일")
                                        .email("enoz12@aipers.net")
                                        .authority(AuthorityType.USER)
                                        .build();
        memberService.addMember(buildMemberInfo);
    }

}