package com.aipers.moon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoonApplication {
	/**
	 * Temp
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(MoonApplication.class, args);
	}

}
