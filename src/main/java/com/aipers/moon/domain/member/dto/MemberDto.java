package com.aipers.moon.domain.member.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
public class MemberDto {
    private Long memberId;

    private String name;

    private String email;

    @JsonFormat(shape = JsonFormat.Shape.STRING ,pattern = "yyyy.MM.dd HH:mm")
    private LocalDateTime createDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING ,pattern = "yyyy.MM.dd HH:mm")
    private LocalDateTime modifyDate;

}
