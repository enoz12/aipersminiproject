package com.aipers.moon.domain.member.repository;

import com.aipers.moon.domain.member.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MemberRepository  extends JpaRepository<Member,Long> {
}
