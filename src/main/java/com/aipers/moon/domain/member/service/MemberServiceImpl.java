package com.aipers.moon.domain.member.service;

import com.aipers.moon.domain.member.dto.MemberDto;
import com.aipers.moon.domain.member.dto.MemberMapper;
import com.aipers.moon.domain.member.entity.Member;
import com.aipers.moon.domain.member.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class MemberServiceImpl implements MemberService{
    private final MemberRepository memberRepository;

    @Override
    public MemberDto getMember(Long memberId) {
        Member findMember = memberRepository.findById(memberId).orElseThrow(() -> new IllegalArgumentException("찾을 수 없는 회원입니다."));
        MemberDto memberDto = MemberMapper.getMapper().memberToMemberDto(findMember);
        log.info("return :: memberDto {}",memberDto);
        return memberDto;
    }

    @Override
    public MemberDto getMemberByName(String MemberName) {
        return null;
    }

    @Override
    @Transactional
    public MemberDto addMember(Member member) {
        Member saveMemberData = memberRepository.save(member);
        return MemberMapper.getMapper().memberToMemberDto(saveMemberData);
    }
}
