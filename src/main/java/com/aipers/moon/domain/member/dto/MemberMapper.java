package com.aipers.moon.domain.member.dto;

import com.aipers.moon.domain.member.entity.Member;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MemberMapper {
    static MemberMapper getMapper(){return Mappers.getMapper(MemberMapper.class);}

    MemberDto memberToMemberDto(Member member);
}
