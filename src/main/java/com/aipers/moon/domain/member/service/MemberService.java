package com.aipers.moon.domain.member.service;

import com.aipers.moon.domain.member.dto.MemberDto;
import com.aipers.moon.domain.member.entity.Member;

public interface MemberService {

    MemberDto getMember(Long memberId);
    MemberDto getMemberByName(String MemberName);

    MemberDto addMember(Member member);


}
