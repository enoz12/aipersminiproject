package com.aipers.moon.domain.member.entity;

import com.aipers.moon.common.entity.BaseTimeEntity;
import com.aipers.moon.domain.member.contans.AuthorityType;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@DynamicUpdate
public class Member extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;
    private String name;
    private String email;
    @Enumerated(EnumType.STRING)
    @Column(name = "authority",columnDefinition = "ENUM('USER','MEMBER','MANAGER','SYSTEM_ADMIN','SUPER_ADMIN')", nullable = false)
    private AuthorityType authority;

    @Builder
    public Member(Long memberId, String name, String email, AuthorityType authority) {
        this.memberId = memberId;
        this.name = name;
        this.email = email;
        this.authority = authority;
    }
}
