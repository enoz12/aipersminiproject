package com.aipers.moon.domain.member.contans;

import lombok.Getter;

import java.util.Arrays;

public enum AuthorityType {
    USER("user","일반"),
    MEMBER("MEMBER","회원"),
    MANAGER("MANAGER","메니저"),
    SYSTEM_ADMIN("SYSTEM_ADMIN","시스템관리자"),
    SUPER_ADMIN("SUPER_ADMIN","슈퍼관리자");

    @Getter
    private final String authCode;
    private final String discription;

    AuthorityType(String authCode, String discription) {
        this.authCode = authCode;
        this.discription = discription;
    }


    public static AuthorityType getAuthCode(String code){
        return Arrays.stream(AuthorityType.values()).filter(v->v.authCode.equalsIgnoreCase(code)).findFirst().orElse(USER);
    }
}
